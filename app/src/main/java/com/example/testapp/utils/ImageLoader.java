package com.example.testapp.utils;

import androidx.annotation.NonNull;

import com.example.testapp.api.ApiFactory;
import com.example.testapp.api.ResponseDataClass;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ImageLoader {

    @NonNull
    public Observable<ResponseDataClass> imageData() {
        return ApiFactory.getApiClass()
                .getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
