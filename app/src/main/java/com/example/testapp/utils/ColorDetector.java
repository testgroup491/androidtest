package com.example.testapp.utils;

import android.graphics.Color;

import java.util.Random;

public class ColorDetector {

    public int getRandomColor() {

        int color = new Random().nextInt();

        return Color.argb(0xff,Color.red(color),Color.green(color),Color.blue(color));
    }
}
