package com.example.testapp.testWork;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.File;


public class TestWork {

    /**
     *
     *
     * Тестовое задание состоит их 4 подзаданий.
     * Среднее время на выполнение задания 3 часа.
     *
     */



    /**
     *
     1) Используя механизм сериализации написать функцию клонирования объекта.
     * @param object
     * @param <T>
     * @return
     */


    public <T> T cloneSerializableObject(@NonNull Object object) {
        return (T)object;
    }

    /**
     2) При помощи регулярных выражений написать функцию разбора и проверки корректности
     подставляемого значения MAC адреса.

     -Подставляемые значения:
     "00:AB:CD:EF:11:22",
     "00-AB-CD-EF-11-22",
     "00ab.cdef.1122",
     "00:AB:CD:EF:11:JK"
     -Функция должна выдать MAC адрес вида "00ABCDEF1122" (в верхнем регистре) если src верный, null в случае ошибки.
     -Необходимо проверять ввод только шестнадцатеричных символов во входной строке и число символов = 12.

     * @param src
     * @return
     */
    public @Nullable String getMAC (@Nullable String src) {
        return null;
    }


    /**
     * 3) Во внутренней памяти устройства находится файл фотографии с камеры. Необходимо написать функцию
     * извлечения Bitmap объекта из файла. Затем этот объект будет напрямую помещен в контейнер ImageView.
     * Не использовать библиотеки работы с изобржениями Glide или Picasso.
     * *
     * @param file
     * @return
     */
    public @Nullable Bitmap getBitmapFromFile(@Nullable File file) {
        return null;
    }


    /**
     * 4.Произвести рефакторинг проекта:
     * 4.1 Используя паттерн MVP архитектуры.
     * 4.2 Используя технологию Dagger2.
     * 4.3 Используя технологии Android Data Binding и ButterKnife(опционально).
     */
}
