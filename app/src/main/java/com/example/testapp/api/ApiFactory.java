package com.example.testapp.api;

import androidx.annotation.NonNull;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public final class ApiFactory {

    public static final String API_ENDPOINT = "https://api.nasa.gov/";
    public static final String API_KEY = "x5HglF0nTdZ2eRBN0pG1xXqjgMRnFPN0VbRyqvhj";

    private static final Api api = buildRetrofit().create(Api.class);

    private ApiFactory() {}

    @NonNull
    public static Api getApiClass() {
        return api;
    }

    @NonNull
    private static Retrofit buildRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(API_ENDPOINT)
                .client(HttpProvider.provideClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}
