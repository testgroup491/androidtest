package com.example.testapp.api;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.testapp.api.ApiFactory.API_KEY;

public final class ApiInterceptor implements Interceptor {

    @NonNull
    public static Interceptor create() {
        return new ApiInterceptor();
    }

    private ApiInterceptor() {}

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder()
                .addQueryParameter("api_key", API_KEY)
                .build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
