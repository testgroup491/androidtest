package com.example.testapp.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DataItem {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public long position;
    public String name;

    public DataItem(long position, String name) {
        this.position = position;
        this.name = name;
    }
}
