package com.example.testapp.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.testapp.R;
import com.example.testapp.adapters.ListViewAdapter;
import com.example.testapp.db.ApplicationDatabase;
import com.example.testapp.db.DataItem;
import com.example.testapp.db.DataItemDao;
import com.example.testapp.utils.ColorDetector;
import com.example.testapp.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.Date;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


@SuppressLint({"CheckResult", "DefaultLocale"})
@SuppressWarnings({"ResultOfMethodCallIgnored", "deprecation"})
public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    private TextView headerText;
    private ImageView imageView,imageTheme;
    private ListView listView;

    private ApplicationDatabase db;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    private final ColorDetector colorDetector = new ColorDetector();
    private final ImageLoader imageLoader = new ImageLoader();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //InitViews:
        headerText = findViewById(R.id.headerText);
        imageView = findViewById(R.id.imageView);
        imageTheme = findViewById(R.id.imageTheme);
        listView = findViewById(R.id.listView);

        Button buttonAdd = findViewById(R.id.add);
        Button buttonRemove = findViewById(R.id.remove);

        //Init listView adapter:
        listView.setAdapter(new ListViewAdapter(this,
                R.layout.list_item_layout));

        //Init and subscribe db:
        db = Room.databaseBuilder(getApplicationContext(),
                ApplicationDatabase.class, "dataBase").build();

        //Get DaoItem
        DataItemDao dataItemDao = db.dataItemDao();

        //Subscribe on dataBase update:
        dataItemDao.getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    ((ListViewAdapter) listView.getAdapter()).updateData(items);
                    headerText.setText(String.format("%s%d", getString(R.string.strT), listView.getAdapter().getCount()));
                    imageView.getDrawable().setTint(colorDetector.getRandomColor());
                });

        //Add item to dataBase:
        buttonAdd.setOnClickListener(v ->
                Observable.fromCallable(() ->
                        dataItemDao.insert(new DataItem(listView.getAdapter().getCount() + 1,
                                new Date().toLocaleString())))
                        .subscribeOn(Schedulers.io())
                        .subscribe());

        //Remove item from dataBase:
        buttonRemove.setOnClickListener(v -> {
            if(listView.getAdapter().getCount() > 0)
                Observable.fromCallable(() ->
                        dataItemDao.delete(((ListViewAdapter)listView.getAdapter())
                                .getItems().get(listView.getAdapter().getCount()-1)))
                        .subscribeOn(Schedulers.io())
                        .subscribe();
        });

        //Load picture into imageView:
        compositeDisposable.add(
                imageLoader.imageData().subscribe(responseDataClass ->
                                Picasso.get()
                                        .load(responseDataClass.getUrl())
                                        .into(imageTheme),
                        throwable ->
                                Log.e(TAG, throwable.getMessage()))
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
        db.close();
    }
}